import React, { useState, useEffect } from 'react';
import './App.css';
import Collection from './collection'
import axios from 'axios'
import { MenuItems } from './MenuItems'

function App() {
const [flashcards, setFlashcards] = useState(SAMPLE_FLASHCARDS)

useEffect(() => {
  axios
  .get('https://opentdb.com/api.php?amount=35&category=15&type=boolean')
  .then(res => {
    setFlashcards(res.data.results.map((questionItem, index)=> {
      return {
        id: `${index}=${Date.now()}`,
        question: decodeString(questionItem.question),
        answer: decodeString(questionItem.correct_answer),

      }
    }))
    console.log(res.data)
  })
}, [])

function decodeString(str){
  const textArea = document.createElement('textarea')
  textArea.innerHTML= str
  return textArea.value
}

  return (
    <>

    <div className="navbar">
      <div className="logo"> 
        <div className="icon"></div>
        <h1 className="navbar-title">WSPA Flashcards</h1>
       </div>
     
      
      <div className="nav-bar-items">
    <ul className="navitem">
        {MenuItems.map((item, index) => {
          return(
            <li key={index}><a className={item.cName} href={item.url}>{item.title}</a></li>
          )

        })}
       
        
    </ul>
       
    </div>


    </div>
    

    <div className="container">
    <div className="collection-title">
      <p>Collection Name</p>
      <p className="collection-edit">Edit Collection</p>
    </div>
    <Collection flashcards ={flashcards} />
    </div>
    
    <div className="footer">
      <p1>
        About us | Contact | Copyright
      </p1>
    </div>

    </>
  );
  
}

const SAMPLE_FLASHCARDS = [
{
  id: 1,
  question: "Loading flashcards.....",
  answer: "N/A"

 
}


]

export default App;
