<div className="navbar">
      <div className="logo"> 
        <div className="icon"></div>
        <h1 className="navbar-title">WSPA Flashcards</h1>
       </div>
     
      
      <div className="nav-bar-items">
    <ul className="navitem">
        {MenuItems.map((item, index) => {
          return(
            <li key={index}><a className={item.cName} href={item.url}>{item.title}</a></li>
          )

        })}
       
        
    </ul>
       
    </div>


    </div>