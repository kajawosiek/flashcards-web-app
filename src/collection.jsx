import React from 'react'
import Card from './card'

export default function collection({ flashcards }) {
    return (
        <div className="card-grid">
            {flashcards.map(flashcard =>{
                return <Card flashcard = {flashcard} key={flashcard.id} />
            })}
            
        </div>
    )
}
