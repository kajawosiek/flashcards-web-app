# Flashcards React Web App

  

This project was bootstrapped with [Create React App] and deployed with Vercel.
it can be found here: https://flashcards-web-app.vercel.app/#

  
  
  

## Version 0.9.1


This version of the project includes a fully functioning flashcard element (currently populated with dummy data), sticky nav-bar, and responsive design. The flashcards are double-sided, and flip correctly on the page of their collection.

Finished elements
* Reversible flash cards
* Hover effects
* Full collection page
* CSS styling to match the Android application
* Sticky navigation bar
  
  ![Collection Page](https://i.imgur.com/A1juJUZ.png)
  ![Collection page with flipped elements](https://i.imgur.com/wFL377r.png)

## To Do

 
* Firebase database sync
* Link additional pages correctly
* Test the functioning database